import pytest
import sys
from PyQt5.QtWidgets import QApplication
from autosync.ui import MainWindow

app = QApplication(sys.argv)

@pytest.fixture
def widget():
    return MainWindow()

def test_main_window_incr_progress(widget):
    '''
    Test it won't crash if progress is not defined, defines it instead
    For #26
    '''
    assert not hasattr(widget, 'progress')
    widget.incr_progress()
    assert hasattr(widget, 'progress')
    assert widget.progress_incr > 0
