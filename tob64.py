import base64
import sys

filename = sys.argv[1]
print('Encoding: ', filename)

with open(filename, 'r') as f:
    contents = f.read()
    contents = contents.encode('utf-8')

contents_b64 = base64.b64encode(contents)
print(contents_b64)
