#!/bin/bash
INSTALL_DIRECTORY='/usr/share/captionpal'
SYMLINK_DIRECTORY='/usr/bin/captionpal'

echo "Installing CaptionPal in $INSTALL_DIRECTORY"

if [ -d "$INSTALL_DIRECTORY" ]; then
  # Clean previous installations
  echo 'Found previous installation. Cleanup.'
  rm -rf "$INSTALL_DIRECTORY"
  echo 'Removing previous symbolic link'
  rm $SYMLINK_DIRECTORY
fi

echo "Creating installation folder: $INSTALL_DIRECTORY"
# Create installation folder
mkdir -p $INSTALL_DIRECTORY

echo "Decompressing binaries into $INSTALL_DIRECTORY"
# Unzip to install folder
tar -xzf captionpal*.tar.gz -C $INSTALL_DIRECTORY

echo "Creating symbolic link to $SYMLINK_DIRECTORY"
# Create symbolic link to nexture inside SYMLINK_DIRECTORY
ln -sf "$INSTALL_DIRECTORY/CaptionPal" $SYMLINK_DIRECTORY

echo "CaptionPal installed successfully."
echo "Run `captionpal --help` to get started with the CLI, or `captionpal` to start the interface"
