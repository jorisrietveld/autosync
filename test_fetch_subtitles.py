import tempfile
import os
from parameterized import parameterized

from autosync.config import __subslangs__
from autosync.ui import fetch_sub_if_found

@parameterized.expand(__subslangs__[:4])
def test_fetch_subtitles_all_langs(lang):
    print(lang)
    with tempfile.TemporaryDirectory() as dir:
        saveto = os.path.join(dir, 'sub.srt')
        info = {
            'type': 'episode',
            'title': 'Game Of Thrones',
            'season': 1,
            'episode': 1,
        }
        assert fetch_sub_if_found(info, lang, save_to=saveto)
